# README #

Welcome to my To-Do-Application!


# How do I get set up? #
_PLEASE READ CAREFULLY_

* You got to run the 2 sides of the Application _and keep it running at the same time_ (Client & server)

* In each folder of these you have a README like this, please go to [./Api](Api) first, follow the steps and check that the server does not have any problems in the console

* There, you have to run some scripts to install some dependencies that the server needs, and the database configuration

* If you get it, got to [./App](App) and follow the steps in the README

* This App is in development!



