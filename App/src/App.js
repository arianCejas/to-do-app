import { useState } from 'react';
import './App.css';
import Dashboard from './components/Dashboard/Dashboard';
import Form from './components/Form/Form'
import Infoboard from './components/Info/Infoboard'

function App() {
  const [loading, setLoading] = useState(false)

  return (
    <div className="App">
      <div className='row'>
      <Form loading={loading} setLoading= {setLoading} />
      <Dashboard loading={loading} setLoading={setLoading} />
      </div>
      <div className='row'>
      <Infoboard />
      </div>
    </div>
  );
}

export default App;
