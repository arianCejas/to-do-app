import React from 'react'

function ShowStats({tasks, dates}) {
    return (
        <div className='mini-row task'>
            <div className='column'>
                <h3>From <span className='date'>{dates.from} </span> 
               to <span className='date'>{dates.to}</span> 
                    <hr/>
               You have {tasks.length} Tasks</h3>
            </div>
            <div className='mini-column'>
                <h4>{tasks.filter(task => task.status == 'pending').length} Tasks <span className='pending'>pending</span></h4>
                <h4>{tasks.filter(task => task.status == 'completed').length} Tasks <span className='completed'>completed</span></h4>

           </div>
        </div>
    )
}

export default ShowStats
