import React from 'react'

function FilteredTasks({tasks}) {
    return (<>
        {tasks.length != 0 && <div className='task-div filter'>
            { tasks.map(task => {
                return (<div className='task '>
                    {task.status == 'pending' ?
                        <>
                        <h2>{task.name}</h2>
                        <span><strong className='pending'>{task.status}</strong> since {task.created_at}</span>
                         </>
                        :
                        <>
                        <del><h2 >{task.name}</h2></del>
                        <span><strong className='completed'>{task.status}</strong> in {task.completed_at}</span>
                         </>
                     }
                </div>)
            })}
        </div>}
        </>
    )
}

export default FilteredTasks
