import React, { useState } from 'react'
import InputDate from './InputDate'
import FilteredTasks from './FilteredTasks'
import ShowStats from './ShowStats'
import OtherInfo from './OtherInfo'

function Infoboard() {
    const [dates, setDates] = useState({
        from : '',
        to : ''
    })
    const [filteredTasks, setFilter] = useState([])
    
    return (
        <div className='big-div'>
            <div className='row'>
                <InputDate setDates={setDates} dates={dates} setFilter={setFilter} />
               {filteredTasks.length != 0 && <ShowStats tasks = {filteredTasks} dates ={dates} />}
            </div>
            <div className='row'>
               <FilteredTasks tasks={filteredTasks} />
                <OtherInfo tasks= {filteredTasks} />
            </div>
        </div>
    )
}

export default Infoboard
