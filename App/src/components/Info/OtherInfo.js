import React, {useEffect} from 'react'
import {getProductiveDay} from '../../functions'

function OtherInfo({ tasks }) { 
    const numDays = tasks.length != 0 && getProductiveDay(tasks.filter(e => e.status == 'completed'))
   if(tasks.length != 0) {
   return ( 
        <div className='task'>
           <h3 className='info'> The most productive day of your week was:</h3>
           <hr/>
            {numDays && <div>
                <h2><strong className='date'>{numDays[0]}</strong> with <label className='completed'>{numDays[1]}</label> Tasks completed!</h2>
                
            </div>}
        </div>
        )
    }
    else return null
}

export default OtherInfo
