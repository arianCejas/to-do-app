import React from 'react'
import { getTasksFromDate } from '../../utils'

function InputDate({setDates, dates, setFilter}) {
    
    const handleChanges = e => {
        setDates({
                ...dates,
                [e.name]: e.value
            }) 
    }
    const handleSubmit =async e => {
        e.preventDefault()
        setFilter(await getTasksFromDate(dates.from, dates.to))
    }
    return (

        <div className='task'>
            <h1> Track your progress over time! </h1>
            <input type='date' name= 'from' onChange={(e) => handleChanges(e.target)}></input>
            <span> to </span>
            <input type='date' name='to' onChange={(e) => handleChanges(e.target)}></input>
            {dates.from && dates.to ? <button onClick={(e) => handleSubmit(e)}>Search</button> : null}
        </div>
    )
}

export default InputDate
