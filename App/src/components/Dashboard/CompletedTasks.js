import React from 'react'
import { deleteTask, undoTask } from '../../utils'

function CompletedTasks({ tasks , setLoading }) {


    const completedTasks = tasks.filter(task => task.status === 'completed')

    const handleClick = (e, id) => {
        e == 'erase' && deleteTask(id) 
        e == 'undo' && undoTask(id)
        setLoading(true)        
    } 
    

    return (
        <div className='task-div'>
               <h1 className='completed'>Completed tasks </h1>
            <hr />
         {completedTasks.length != 0 ? <span>You have {completedTasks.length} completed tasks</span> : <span>There are not completed tasks...</span>}
             {completedTasks && completedTasks.map(task => {
                 return (<ul className='task'>
                    <div className='mini-row'>
                         <del><h3>{task.name} </h3></del>
                         <input type='checkbox' checked='true' onClick={(e) => handleClick('undo', task.id)}></input>
                         <button onClick={(() => handleClick('erase', task.id))}>X</button>
                    </div>
                    <span>{task.day} </span>
                    <span><strong  className='completed'>{task.status}</strong> in {task.completed_at}</span>
             
                    </ul>
                )
            })}
        </div>
    )
}

export default CompletedTasks
