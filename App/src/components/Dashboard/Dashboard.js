import {useEffect, useState} from 'react'
import { getTasks } from '../../utils'
import CompletedTasks from './CompletedTasks'
import UncompletedTasks from './UncompletedTasks'

function Dashboard({loading, setLoading}) {
    const [tasks, setTasks] = useState([])

    useEffect(async() => {
        setTasks(await getTasks())
        !!loading &&  setLoading(false)

    }, [loading])

    return (
        <div className='column'>
            <h1 className='title'>Last To-do's from 7 days ago  </h1>
        <div className='row'>
            <UncompletedTasks tasks={tasks} setLoading={setLoading}/> 
            <CompletedTasks tasks={tasks} setLoading={setLoading}/>
            </div>
            </div>
    )
}

export default Dashboard
