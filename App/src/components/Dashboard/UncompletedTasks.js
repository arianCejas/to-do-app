import { completeTask, deleteTask } from "../../utils"

function UncompletedTasks({ tasks, setLoading }) {
      
    const uncompletedTasks = tasks.filter(task => task.status === 'pending')
     
    const handleClick = (e, id) => {
        e == 'erase' && deleteTask(id)
        e == 'complete' && completeTask(id)
        setLoading(true)        
    } 
    
    return (
        <div className='task-div'>
            <h1 className='pending'>Pending tasks </h1>
            <hr/>
            {uncompletedTasks.length != 0 ? <span>You have {uncompletedTasks.length} pending tasks</span> : <span>There are not pending tasks...</span>}
            {uncompletedTasks && uncompletedTasks
                .map(task => {
                return (<div className='task'>
                     <div className='mini-row'>
                        <h3>{task.name} </h3>
                        <input type='checkbox' className='mini-row'  onClick={(e) => handleClick('complete', task.id)}></input>
                         <button onClick={(e) => handleClick('erase', task.id)}>X</button>
                       </div>
                    <span>{task.created_at} </span>
                    <span>{task.day}</span>
                    </div>
                )})}
        </div>
    )
}

export default UncompletedTasks
