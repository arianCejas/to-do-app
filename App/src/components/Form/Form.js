import {useState} from 'react'
import { addTask, getTasks } from '../../utils'


function Form({loading, setLoading}) {
    const [task, setTask] = useState({})
    const handleChanges = e => {
        setTask({
            [e.name]: e.value
        })
    }
    const handleSubmit = e => {
        e.preventDefault()
        addTask(task)
        setLoading(true)
    }

    return (
        <form className='form-div'>
            <h1>Add a To-Do</h1>
            <input type='text' placeholder='task' name='name' onChange={(e) => handleChanges(e.target)}></input>
            <button onClick={handleSubmit}>Add</button>
        </form>
    )
}

export default Form
