const getProductiveDay = array  => {
    let flag = {
        Monday: 0,
        Tuesday: 0,
        Wednesday: 0,
        Thursday: 0,
        Friday: 0,
        Saturday: 0,
        Sunday: 0
    }
    array.forEach(task => {
        let word = task.completed_at && task.completed_at.split(",")[0]
        flag[word] = flag[word] + 1
    })
     console.log(flag)
    var max = Object.entries(flag).sort((a, b) => {
        return b[1] - a[1]
    })
     return max[0]
}

export {
    getProductiveDay
}