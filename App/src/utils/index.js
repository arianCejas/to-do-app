const axios = require('axios')
const config = require('../config')

const getTasks = async() => {
    return axios.get(`http://localhost:3001/api/todo/`)
    .then(res => res.data)
}
const getTasksFromDate = async(from, to) => {
    return axios.get(`http://localhost:3001/api/todo/from?from=${from}&to=${to}`)
    .then(res => res.data)
}
const addTask = async task => {
    return await axios.post(`http://localhost:3001/api/todo/post`, {
        name: task.name,
        day: task.day
    }) 
}
const completeTask = async id => {
    return await axios.put(`http://localhost:3001/api/todo/update`, {
        id,
        status:"completed"
    }) 
}
const undoTask = async id => {
    return await axios.put(`http://localhost:3001/api/todo/update`, {
        id,
        status:"pending"
    }) 
}
const deleteTask = async id => {
    return await axios.delete(`http://localhost:3001/api/todo/delete`, {
        data:
            { id }
    })
}

export {
    addTask,
    getTasks,
    getTasksFromDate,
    completeTask,
    deleteTask,
    undoTask
}