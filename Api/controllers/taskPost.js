const { Task } = require('../db')


exports.postTask = (req, res, next) => {
    const { name} = req.body
    Task.create({ name})
        .then(response => res.json("Tarea Creada"))
        .catch(error => next(error))
}