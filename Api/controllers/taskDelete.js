const { Task } = require('../db')

exports.deleteTask = (req, res, next) => {
    const { id } = req.body
    Task.destroy({where:{
        id : id
     }})
        .then(result => res.json("Deleted Task"))
        .catch(error => next(error))
}