const { Task } = require('../db')
const { Op } = require('sequelize');
const moment = require('moment') 


exports.getTasks = async (req, res) => {

    const tasks = await Task.findAll({
        where: {
            created_at: {
              [Op.gte]: moment().subtract(7, 'days').toDate()
            }
          }
    })
        res.json(tasks)
 
}

exports.getTasksFromDate = async (req, res) => {
    const { from, to } = req.query

    const tasks = await Task.findAll({where : {created_at : {[Op.between] : [from , to ]}}})
    res.json(tasks)
}