const { getTasks, getTasksFromDate } = require('./taskGet')
const { postTask } = require('./taskPost')
const { updateTask } = require('./taskUpdate')
const { deleteTask } = require('./taskDelete')


exports.controllers = {
     getTasks,
     getTasksFromDate,
     postTask,
     updateTask,
     deleteTask
}
