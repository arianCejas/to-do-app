const {Task} = require ('../db')

exports.updateTask = async (req, res, next) => {
    const { id, status } = req.body
    
     const task = status === 'completed' ? await Task.update({
        status: status,
        completed_at: Date.now()
    }, {
        where:{
            id: id
        }
    }) : await Task.update({
        status: status,
        completed_at: null
    }, {
        where:{
            id: id
        }
    })
        .then(response => res.json('Task Status succesfully changed'))
        .catch(error => next(error))
}