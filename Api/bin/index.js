
const server = require('../app.js');
const { conn } = require('../db.js');

// Syncing all the models at once.
conn.sync({ force: false }).then(() => {
  server.listen(3001, () => {
    console.log('Abierto en puerto 3001'); // eslint-disable-line no-console
  });
});
