var express = require('express');
var router = express.Router();
const { todoValidation } = require('../middlewares')
const { controllers }  = require('../controllers')

router.get('/todo', controllers.getTasks);
router.get('/todo/from', controllers.getTasksFromDate)

router.post('/todo/post', todoValidation, controllers.postTask);

router.put('/todo/update', controllers.updateTask);

router.delete('/todo/delete', controllers.deleteTask);

module.exports = router;
