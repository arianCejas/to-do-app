const { DataTypes } = require('sequelize');


module.exports = (sequelize) => {

    sequelize.define('Task', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        created_at: {
            type: DataTypes.DATE,
            defaultValue: new Date,
            get() {
                const rawValue = this.getDataValue("created_at");
                return rawValue.toLocaleString('en-US', {
                    weekday: 'long',
                    day: 'numeric',
                    year: 'numeric',
                    month: 'long',
                    hour: 'numeric',
                    minute: 'numeric',
                    second: 'numeric',
                })
            }
        },
        completed_at: {
            type: DataTypes.DATE,
            get() {
                const rawValue = this.getDataValue("completed_at");
                if(rawValue){
                return rawValue.toLocaleString('en-US', {
                    weekday: "long",
                    day: 'numeric',
                    year: 'numeric',
                    month: 'long',
                    hour: 'numeric',
                    minute: 'numeric',
                    second: 'numeric',
                })}else return null
            }
        },
        status: {
            type: DataTypes.ENUM("pending", "completed"),
            defaultValue: "pending"
        }

    }, {
        createdAt: false,
        updatedAt: false
    });

};
