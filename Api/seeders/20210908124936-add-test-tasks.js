'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Tasks', [
      {
        name: 'go to the gym',
        created_at: new Date,
        completed_at: "2021-09-13",
        status:"pending"
      },
      {
        name: 'go to the mall',
        created_at: new Date,
        completed_at: "2021-09-13",
        status:"pending"
      },
      {
        name: 'go visit my mother',
        created_at: "2021-07-01",
        completed_at: "2021-09-06",
        status:"completed"
      },
      {
        name: 'clean my room',
        created_at: "2021-08-01",
        completed_at: "2021-09-06",
        status:"completed"
      },
      {
        name: 'paint my room',
        created_at: "2021-06-01",
        completed_at: "2021-09-13",
        status:"completed"
      },
      {
        name: 'clean the dishes',
        created_at: "2021-06-10",
        completed_at: "2021-09-09",
        status:"completed"
      },
      {
        name: 'fix the TV',
        created_at: "2021-07-01",
        completed_at: "2021-09-10",
        status:"completed"
      },
      {
        name: 'build a tree house',
        created_at: "2021-09-11",
        completed_at: null,
        status:"pending"
      },

    ], {});
    

  },

  down: async (queryInterface, Sequelize) => {
  
     await queryInterface.bulkDelete('Tasks', null, {});
     
  }
};
