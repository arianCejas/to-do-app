# README #



# SERVER SIDE #
_PLEASE READ CAREFULLY_

* to install dependencies, first run 
``` bash
npm install
```
* Configure the .env.example following the steps, then erase ".example" of the filename (e.g: ".env")
_Please notice that if you don´t have the DB created, create it in your SQL console_


* After that, run
``` bash
npx sequelize-cli db:seed:all
```
_this is for add some to-do's to de DB_

* Finally, if everything is OK, run:
``` bash
npm start
```


* Look in you console, if there are not problems, yo got to see 
'Abierto en puerto 3001'

### go to [../App](App) and view the README!

