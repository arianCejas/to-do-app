const { check, validationResult } = require('express-validator');

const checkValidations = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const error = {
        message: "Validation error",
        code: 400,
        detail: errors.errors,
      };
  
      throw error;
    }
    next();
};
  
const nameNotEmpty = check("name", "name field is empty").notEmpty();

exports.todoValidation = [
    nameNotEmpty,
    checkValidations,
];